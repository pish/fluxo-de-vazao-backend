const functions = require('firebase-functions');
const admin = require('firebase-admin');

const databasePath = "/Dispositivos/0Wes0qhmHoDybRM5Mt97/Registros/{recordId}"

admin.initializeApp(functions.config().firebase); //Inicializar o Firebase com configurações do functions

exports.sendFlowRate = functions.https.onRequest((req, res) => {
    if (!isNaN(req.body.vazao) && req.body.vazao !== null) {
        const result = admin.firestore()
            .doc("/Dispositivos/0Wes0qhmHoDybRM5Mt97")
            .collection("Registros")
            .add({
                vazao_de_agua: req.body.vazao + "",
                time: Date.now()
            })

        result.then(response => {
            console.info(response)
            return res.status(201).send()
        }).catch(error => {
            console.warn(error)
            return res.status(500).send()
        })
    } else {
        return res.status(422).send()
    }
})

exports.sendNotificationWhenRegisterIsWrited = functions.firestore.document(databasePath).onWrite(async (event) => {
    admin.firestore()
        .doc("/Dispositivos/0Wes0qhmHoDybRM5Mt97")
        .get()
        .then(result => {
            return sendNotification(result.data().fcmToken, event.after.data().vazao_de_agua)
        })
        .then(response => console.info(response))
        .catch(error => console.warn(error))
})

const sendNotification = (fcmToken, flowRate) => admin.messaging().send(
    {
        data: {
            vazao_de_agua: flowRate
        },
        token: fcmToken
    }
)
